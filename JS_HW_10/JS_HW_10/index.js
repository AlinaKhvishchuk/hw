const tabs = document.querySelectorAll('.tabs-title');
const tabsHeroes = document.querySelectorAll('.heroes');
tabs.forEach((currentTab) => {
	currentTab.addEventListener('click', (evt) => {
		tabs.forEach((tab) => {
			if (tab.classList.contains('active')) {
				tab.classList.remove('active');
			}
		});
		evt.currentTarget.classList.add('active');

		const attr = currentTab.getAttribute('data-hero');
		tabsHeroes.forEach((tabHero) => {
			let containsClass = tabHero.classList.contains(attr);
			if (containsClass) {
				tabHero.classList.add('active');
			} else {
				tabHero.classList.remove('active');
			}
		});
	});
});