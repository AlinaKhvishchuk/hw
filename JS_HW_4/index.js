// 1. Якщо нам потрібно виконувати схожі або такіж самі діі декілька разів, то ми викорстовуємо функцію.Функція виокремлює шматок коду та надає йому ім'я, його можна використувавати будь-де в коді.

//2. Аргумент - це локальна змінна, з якою працює функція. Коли ми передаємо ці значення функціі, то вона іх обробляє та повертає результат.

//3. Оператор return зупиняє роботу функціі та повертає отримане значення. Без нього функція може виконувати бажані діі але не повертати отримане значення.

function calculate(num1, num2, mathOperation) {
	switch (mathOperation) {
		case "+":
			return num1 + num2;
		case "-":
			return num1 - num2;
		case "*":
			return num1 * num2;
		case "/":
			return num1 / num2;
		default:
			return "Math operation is incorrect";
	}
}

function isValidNumbers (num1, num2) {
	return !(isNaN(num1) || isNaN(num2));
}

function calculateNumbers () {
	let num1 = Number(prompt("Insert your first number"));
	let num2 = Number(prompt("Insert your second number"));
	let isValid = isValidNumbers(num1,num2);

	while (!isValid) {
		num1 = Number(prompt("Insert your first number"));
		num2 = Number(prompt("Insert your second number"));
		isValid = isValidNumbers(num1,num2)
	}

	const mathOperation = prompt("Insert math operation");
	const result = calculate(num1, num2, mathOperation);
	console.log(result);
}

calculateNumbers();