// Прототипне наслідування - це можливість JS не копіювати властивості та
// методи об'єктів, якщо вони мають багато спільного, а використовувати базовий
// об'єкт (класс) за основу та додавати йому інші, індивідуальні властивості.
// Наприклад, маємо об'єкт Автомобіль (він має кермо, двигун, 4 колеса іті). На
// його базі створюємо об'єкт Opel Astra (де вже будуть вказані потужність саме
// цього двигуна, вид топлива, кількість дверей та будь-що інше). А потім на
// базі цього можна створити ще один, більш персоналізований автобіль.

// Super - викорстовується у двох випадках: 1) під час виклику конструктору
// дочірнього класу, щоб вказати які саме властивості ми хочемо використовувати
// з батьківського класу. 2) для виклику батьківських методів.

class Employee {
	constructor(name, age, salary) {
		this.name = name;
		this.age = age;
		this.salary = salary;
	}
	get employeeName() {
		return this.name;
	}
	set employeeName(newName) {
		this.name = newName;
		console.log(this.name);
	}
	get employeeAge() {
		return this.age;
	}
	set employeeAge(newAge) {
		this.age = newAge;
		console.log(this.age);
	}
	get employeeSalary() {
		return this.salary;
	}
	set employeeSalary(newSalary) {
		this.salary = newSalary;
		console.log(this.salary);
	}
}

const employee1 = new Employee('Alina', 30, 15000);
console.log(employee1);
console.log(employee1.employeeName);
employee1.employeeName = 'Viktor';
console.log(employee1);
console.log(employee1.employeeSalary);
employee1.employeeSalary = '25000';
employee1.employeeAge = '27';
console.log(employee1.employeeAge);

class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary);
		this.lang = lang;
	}
	 get employeeSalary() {
		return this.salary = this.salary*3;
	 }
}

const programmer1 = new Programmer('Mark', 27, 45000, ['English', 'German']);
console.log(programmer1);
console.log(programmer1.employeeSalary);


const programmer2 = new Programmer('Jack', 54, '1250', ['English', 'Ukrainian', 'French']);
console.log(programmer2);
programmer2.employeeName = 'John';
console.log(programmer2);

const programmer3 = new Programmer('Steven', 39, 3400, ['French', 'German']);
console.log(programmer3);
