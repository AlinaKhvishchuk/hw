let body = document.getElementById('body');
let btnThemeChange = document.getElementById('theme-change');

btnThemeChange.addEventListener('click', () => {
	body.classList.toggle('dark');
	if (body.classList.contains('dark')) {
		localStorage.setItem('theme', 'dark');
	} else {
		localStorage.setItem('theme', 'light');
	}
});

function usePreviousTheme() {
	let currentTheme = localStorage.getItem('theme');
	switch (currentTheme) {
		case 'dark':
			body.classList.add('dark');
			break;
		case 'light' :
			body.classList.remove('dark');
			break;
	}
}

addEventListener('DOMContentLoaded', () => {
	usePreviousTheme();
});
