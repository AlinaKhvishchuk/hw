// // 1. Основні типи даних:
// - number (цілі числа, або числа з плаваючою комою);
// - string (рядок з послідовністю символів);
// - boolean (логічний тип даних. Можливо тільки два варіанти: true / false);
// - undefined (змінна, якій не призначили тип даних);
// - null (пусте значення).

// 2. == - звичайне рівняння. Нечислові типи даних можуть приводитися до них, наприклад false == 0. А === - суворе рівняння. Видає false, якщо порівнюємі змінні належать до різних типів даних.

// 3. Оператор - це вбудована в js функція.

let name = prompt("What is your name?");
let age = prompt ("What is your age?");
function validAgeAndNumber(name,age) {
	const ageNumber = Number(age);
	return !(isNaN(ageNumber) || !name);
}
let isValid = validAgeAndNumber(name, age);
while (!isValid) {
	name=prompt("What is your name?",name);
	age=prompt ("What is your age?",age);
	isValid = validAgeAndNumber(name, age);
}
	switch (true) {
		case age < 18:
			alert ("You are not allowed to visit this website");
			break;
		case age>=18 && age <= 22:
			let confirmation=confirm("Are you sure you want to continue?");
			if (confirmation) {
				alert ("Welcome,"+name);
			}
			break;
		case age > 22:
			alert ("Welcome, "+name);
			break;
		default:
			alert ("I can't decide if I should let you in")
	}
