// Під часи роботи з input не рекомендують використовувати клавіатуру, тому що є багато різних способів ввести щось у поле. Можна викорситовувати подію input для того, щоб урахувати усі можливі варіанти вводу даних.

document.addEventListener('keydown', (evt) => {
	let btn = document.querySelectorAll(".btn");
	[...btn].forEach((currentBtn) => {
		currentBtn.classList.remove('active');
		let currentBtnTxt = currentBtn.innerText;
		const enteredKey = evt.key.toLowerCase();
		const keyboardKey = currentBtnTxt.toLowerCase();
		if (enteredKey === keyboardKey) {
			currentBtn.classList.toggle('active');
		}
	})
})