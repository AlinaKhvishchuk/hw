// 1.Метод об'єкту - це функція в середині об'єкту, яка робить операціі з іншими властивостями функціі 

//2. Значення властивості може бути будь-якого типу даних

//3. Якщо я створю об'єкт a, а потім b = a. То об'єкт b не буде містити копію властивостей першого об'єкту, а буде містити посилання властивості першого.



function User(firstName, lastName) {
	// this.firstName = firstName;
	// this.lastName = lastName;
	this.getLogin = function() {
		let login = this.firstName.slice(0,1) + this.lastName;
		return login.toLowerCase()
	}

	Object.defineProperty(this, 'firstName', {
			writtable: false,
			configurable: true,
			value: firstName,
	})

	Object.defineProperty(this, 'lastName', {
		writtable: false,
		configurable: true,
		value: lastName,
	})
}

let user = new User (prompt("Enter your first name"), prompt("Enter your last name"));
console.log(user);
console.log(user.getLogin());
user.firstName = "ALina";
user.lastName = "Shevchenko"
console.log(user)
