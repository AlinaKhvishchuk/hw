// Our services TABS
const tabs = document.querySelector('.tabs');
const ourServicesTabs = document.querySelectorAll('.our-services-tab');
tabs.addEventListener('click', (evt) => {
	let target = evt.target;
	let tabActive = document.querySelector('.tabs-title.active');
	tabActive.classList.remove('active');
	target.classList.add('active');
	const attr = target.getAttribute('data-name');
	[...ourServicesTabs].forEach((tab) => {
		let containsClass = tab.classList.contains(attr);
		if (containsClass) {
			tab.classList.add('active');
		} else {
			tab.classList.remove('active');
		}
	});
});

// OUR AMAZING WORK FILTER
let ourWorkTabs = document.querySelector('.our-amazing-work-tabs');
let samples = document.querySelectorAll('.sample');
ourWorkTabs.addEventListener('click', (evt) => {
	let target = evt.target;
	let tabActive = document.querySelector('.our-amazing-work-tabs-title.active');
	tabActive.classList.remove('active');
	target.classList.add('active');
	const attr = target.getAttribute('data-category');
	[...samples].forEach((sample) => {
		let containsClass = sample.classList.contains(attr);
		let isAll = attr === 'all';
		if (containsClass || isAll) {
			sample.classList.remove('hidden');
		} else {
			sample.classList.add('hidden');
		}
	});
});

let btnLoadMore = document.querySelector('.load-more');
let allSamples = document.querySelector('.our-amazing-work-samples');
btnLoadMore.addEventListener('click', () => {
	let firstClick = allSamples.classList.contains('first-click');
	let secondClick = allSamples.classList.contains('second-click');
	if (!firstClick && !secondClick) {
		allSamples.classList.add('first-click');
	} else {
		allSamples.classList.add('second-click');
		btnLoadMore.classList.add('hidden');
	}
});

//CAROUSEL


function showActiveCustomer(target) {
	const attr = target.getAttribute('data-customer');
	[...feedback].forEach((feed) => {
		let containsClass = feed.classList.contains(attr);
		if (containsClass) {
			feed.classList.add('active');
		} else {
			feed.classList.remove('active');
		}
	});
}

function removeAnimationClasses() {
	bigAvatarsArray.forEach(el => {
		el.classList.remove('animation-right');
		el.classList.remove('animation-left');
	})
}

let smallAvatars = document.querySelector('.small-carousel');
let allSmallAvatars = document.querySelectorAll('.small-carousel-avatar');
let avatarsArray = [...allSmallAvatars];
let allBigAvatars = document.querySelectorAll('.feedback');
let bigAvatarsArray = [...allBigAvatars];
let feedback = document.querySelectorAll('.feedback');


smallAvatars.addEventListener('click', (evt) => {
	let target = evt.target.closest('.small-carousel-avatar');
	if (!target) return;
	let avatarActive = document.querySelector('.small-carousel-avatar.active');
	let indexActive = avatarsArray.indexOf(avatarActive);
	let indexTarget = avatarsArray.indexOf(target);
	avatarActive.classList.remove('active');
	let targetContainsClass = target.classList.contains('active');
	if (targetContainsClass) return;
	target.classList.add('active');
	target.classList.remove('animation-right');
	target.classList.remove('animation-left');
	if (indexTarget>indexActive) {
		bigAvatarsArray[indexTarget].classList.add("animation-left");
	} else {
		bigAvatarsArray[indexTarget].classList.add('animation-right');
	}
	setTimeout(() => {
		showActiveCustomer(target);
	}, 0);
});

let leftArrow = document.querySelector('.arrow.left');
leftArrow.addEventListener('click', () => {
	removeAnimationClasses();
	let smallAvatarActive = document.querySelector('.small-carousel-avatar.active');
	let index = avatarsArray.indexOf(smallAvatarActive);
	let previousAvatar;
	smallAvatarActive.classList.remove('active');
	if (index > 0) {
		previousAvatar = avatarsArray[index - 1];
		bigAvatarsArray[index - 1].classList.add('animation-right');
	} else {
		previousAvatar = avatarsArray[avatarsArray.length - 1];
		bigAvatarsArray[avatarsArray.length - 1].classList.add('animation-right');
	}
	previousAvatar.classList.add('active');
	setTimeout(() => {
		showActiveCustomer(previousAvatar);
	}, 0);
});

let rightArrow = document.querySelector('.arrow.right');
rightArrow.addEventListener('click', () => {
	removeAnimationClasses();
	let smallAvatarActive = document.querySelector('.small-carousel-avatar.active');
	let index = avatarsArray.indexOf(smallAvatarActive);
	let nextAvatar;
	smallAvatarActive.classList.remove('active');
	if (index >= (avatarsArray.length - 1)) {
		nextAvatar = avatarsArray[0];
		bigAvatarsArray[0].classList.add('animation-left');
	} else {
		nextAvatar = avatarsArray[index + 1];
		bigAvatarsArray[index+1].classList.add('animation-left');
	}
	nextAvatar.classList.add('active');

	setTimeout(() => {
		showActiveCustomer(nextAvatar);
	}, 0);
});


