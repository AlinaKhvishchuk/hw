// 1.

//2. Засоби оголошення функціі: Function declaration (function SayHello() {alert("Hello")};); function expression (const SayHello = function () {alert("Hello");}.

//3.

function User(firstName, lastName, birthday) {
	this.firstName = firstName;
	this.lastName = lastName;
	this.birthday = birthday;
	this.year = this.birthday.slice(6);
	this.month = this.birthday.slice(3,5);
	this.getLogin = function() {
		let login = this.firstName.slice(0, 1) + this.lastName;
		return login.toLowerCase()
	}
	this.getAge = function() {
		let today = new Date();
		const yearsDifference = today.getFullYear() - this.year;
		return today.getMonth() < this.month
			? yearsDifference - 1
			: yearsDifference
	}
	this.getPassword = function() {
		return (this.firstName.slice(0, 1).toUpperCase() + this.lastName.toLowerCase() + this.year)
	}
}
let user = new User (prompt("Enter your first name"), prompt("Enter your last name"), prompt("Enter your date of birth in format dd.mm.yyyy"));
console.log(user);
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
