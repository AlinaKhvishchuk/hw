//1. DOM - це програмний інтерфейс, який дозволяє проводити операціі з елементами сторінки (видаляти іх, додавати, змінювати стилі і т.і)
//2. innerHTML - дозволяє повертати та змінювати будь-які частини,що входять до цього елементу, а innerText - тільки текстові.
//3. Звернутися до елемента сторінки можна за допомогою querySelector та його варіаціі, за допомогою getElementbyName(Class, Id etc.)

let paragraphs = document.querySelectorAll('p');
console.log(paragraphs);
[...paragraphs].forEach((elem) => {
	elem.style.backgroundColor = "#ff0000";
});
let optionsList = document.getElementById('optionsList');
console.log(optionsList);
let parent = optionsList.parentNode;
console.log(parent);
let children = optionsList.children;
console.log(children);
let testParagraph = document.getElementById('testParagraph');
testParagraph.innerText = 'This is a paragraph';
let mainHeader = document.querySelector('.main-header');
console.log(mainHeader);
let mainHeaderChildren = mainHeader.children;
console.log(mainHeaderChildren);
[...mainHeaderChildren].forEach((elem) => {
	elem.classList.add('nav-item');
});
console.log(mainHeaderChildren);
let sectionTitle = document.querySelectorAll('.section-title');
console.log(sectionTitle);
[...sectionTitle].forEach((elem) => {
	elem.classList.remove('section-title');
});
console.log(sectionTitle);

