//1. Метод forEach перебирає елементи масиву. Це аналог for, але більш красивий та елегантний.
//2. arr.length=0;
//3. Для того, щоб дізнатися чи є та або інша змінна масивом треба використовувати метод Array.isArray()


// Варіант 1
// function filterBy(arr, dataType) {
// 	let filteredItems = [];
// 		arr.forEach((arrItem) => {
// 					if (!checkIsTypeEqual(dataType, arrItem)) {
// 							filteredItems.push(arrItem);
// 						}
// 					// else filteredItems.push(arrItem);
// 				})
// 			return filteredItems;
// }

//Варіант 2
function  filterBy(arr, dataType) {
	return arr.filter((arrItem) => !checkIsTypeEqual(dataType, arrItem))
}

	function checkIsTypeEqual(dataType, item) {
		switch(dataType) {
			case NaN:
				return isNaN(dataType) && isNaN(item);
			case null:
				return dataType === null && item === null;
			default:
				return typeof item === dataType;
		}

		}
console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));
console.log(filterBy(['hello', 'world', 23, '23', null], 'number'));
console.log(filterBy(['hello', 'world', 23, '23', null], null));

