// Конструкцію try...catch доречно використовувати, коли ми очкікуємо отримати якісь дані з серверу, але вони можуть не надіти або може виникнути якась інша помилка, яка потенційно може "зламати" весь наш код. Також доречно використовувати таку конструкцію, якщо користувач має ввести якісь дані.


const books = [
	{
		author: "Люсі Фолі",
		name: "Список запрошених",
		price: 70
	},
	{
		author: "Сюзанна Кларк",
		name: "Джонатан Стрейндж і м-р Норрелл",
	},
	{
		name: "Дизайн. Книга для недизайнерів.",
		price: 70
	},
	{
		author: "Алан Мур",
		name: "Неономікон",
		price: 70
	},
	{
		author: "Террі Пратчетт",
		name: "Рухомі картинки",
		price: 40
	},
	{
		author: "Анґус Гайленд",
		name: "Коти в мистецтві",
	},
];


const root = document.getElementById('root');
const list = document.createElement('ul');
root.appendChild(list);
console.log(list);

books.forEach(book => {
	try {
		if (book.name && book.author && book.price) {
			const listItem = document.createElement('li');
			listItem.innerText = `Author: ${book.author}, name: ${book.name}, price: ${book.price}`;
			list.appendChild(listItem);
		} else if (!book.name) {
			throw `Властивості \"name\" немає`;
		} else if (!book.author) {
			throw `Властивості \"author\" немає`;
		} else if (!book.price) {
			throw `Властивості \"price\" немає`;
		}
	} catch (e) {
		console.log(e);
	}
})
