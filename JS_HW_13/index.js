let img = document.querySelector(".image-to-show");
let imgs = document.querySelectorAll(".image-to-show");
const images = [];
function createImagesArray() {
	[...imgs].forEach((elem) => {
		images.push(elem.src);
	})
}
createImagesArray();

let i=1;
function changeImg() {
	if (i<imgs.length) {
		img.src = images[i];
		i++;
	} else {
		i=0;
		img.src = images[i];
		i++;
	}
}
let intervalId
function interval() {
	intervalId = setInterval(changeImg, 3000);
}
interval();
let btnStop = document.createElement("button");
btnStop.innerHTML = "Припинити";
document.body.append(btnStop);
btnStop.addEventListener('click', (evt) => {
	clearInterval(intervalId);
	intervalId = 0;
})

let btnRestart = document.createElement('button');
btnRestart.innerHTML = "Відновити показ";
document.body.append(btnRestart);
	btnRestart.addEventListener('click', (evt) => {
		if (!intervalId) {
			interval();
		}
	})