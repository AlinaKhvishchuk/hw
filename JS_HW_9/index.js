// 1. Створити HTML тег на сторінці можна за допомогою document.createElement().
// 2. Перший параметр функціі insertAdjacentHTML означає позицію передану в стрінговому форматі, куди вставляється текст ("afterbegin", "beforeend" etc.).
// 3. Для того, щоб видалити елемент со сторінки необхідно використовувати метод .removeChild


function arrayToList(arr, parent = document.body) {
	let ol = document.createElement('ol');
	arr.forEach((elem) => {
		let li= document.createElement('li');
		li.innerHTML = elem;
		ol.appendChild(li);
	})
	parent.appendChild(ol);
}

arrayToList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], document.querySelector('.list'))