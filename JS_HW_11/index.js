// function checkPassword(password1, password2) {
// 	return password1 === password2 ? true : false
// };
//
// console.log(checkPassword(222,7777));


function equalPassword() {
	let pass1 = document.getElementById("password").value;
	let pass2 = document.getElementById("password-check").value;
	return pass1 === pass2
}

let btn = document.getElementById("submitBtn");
btn.addEventListener('click', (evt) => {
	evt.preventDefault();
	let isEqual = equalPassword();
	if (isEqual) {
		window.alert("You are welcome");
	} else {
		document.getElementById("password").value = "";
		document.getElementById("password-check").value = "";
		document.getElementById("error-text").style.display="block"
	}
})
// let iconPassword = document.querySelectorAll(".icon-password");
// [...iconPassword].forEach((elem) => {
// 	elem.addEventListener('click', () => {
// 		elem.classList.toggle("fa-eye");
// 		elem.classList.toggle("fa-eye-slash");
// 		let parent = elem.closest(".input-wrapper");
// 		let passElem = parent.querySelector(".password");
// 		if (elem.classList.contains("fa-eye")) {
// 			passElem.type = "password";
// 		} else {
// 			passElem.type = "text";
// 		}
// 	})
// })

let form = document.querySelector('[data-name="password-form"]');
console.log(form);
form.addEventListener('click',(evt) => {
	let target = evt.target;
	if (!target.classList.contains('icon-password')) return;
	target.classList.toggle("fa-eye");
	target.classList.toggle("fa-eye-slash");
	let parent = target.closest(".input-wrapper");
	let passElem = parent.querySelector(".password");
	if (target.classList.contains("fa-eye")) {
		passElem.type = "password";
	} else {
		passElem.type = "text";
	}
})